/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef void (*handle_meta_t)(const char *key, const char *value);

typedef struct
{
	const char *name;
	const char *desc;
	handle_meta_t handler;
} Command;

void handlePkgList(const char *key, const char *value)
{
	if (strcmp(key, "mbs.pkg") == 0)
	{
		printf("%s\n", value);
	};
};

static Command commandList[] = {
	{"pkg-list", "List the packages in this build.", handlePkgList},
	
	// LIST TERMINATOR
	{NULL, NULL, NULL}
};

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		fprintf(stderr, "USAGE:\n");
		
		Command *scan;
		for (scan=commandList; scan->name!=NULL; scan++)
		{
			fprintf(stderr, "\t%s %s  # %s\n", argv[0], scan->name, scan->desc);
		};
		
		return 1;
	};
	
	const char *cmd = argv[1];
	if (strcmp(cmd, "--version") == 0)
	{
		fprintf(stderr, "Madd Build System %s\n", MBS_VERSION);
		fprintf(stderr, "Copyright (c) 2018, Madd Games. All rights reserved.\n");
		return 0;
	};
	
	Command *scan;
	handle_meta_t handler = NULL;
	for (scan=commandList; scan->name!=NULL; scan++)
	{
		if (strcmp(scan->name, cmd) == 0)
		{
			handler = scan->handler;
			break;
		};
	};
	
	if (handler == NULL)
	{
		fprintf(stderr, "%s: unknown command `%s': run me without arguments for help\n", argv[0], cmd);
		return 1;
	};
	
	FILE *fp = fopen("mbs.meta", "r");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: cannot open mbs.meta: %s\n", argv[0], strerror(errno));
		return 1;
	};
	
	char linebuf[2048];
	char *line;
	while ((line = fgets(linebuf, 2048, fp)) != NULL)
	{
		char *endline = strchr(line, '\n');
		if (endline != NULL) *endline = 0;
		
		char *key = strtok(line, " \t");
		char *value = strtok(NULL, "");
		if (key != NULL && value != NULL)
		{
			handler(key, value);
		};
	};
	
	return 0;
};