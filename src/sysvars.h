/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SYSVARS_H_
#define SYSVARS_H_

/**
 * This header defines system variables. Please define macros as follows:
 *
 * MBS_EXT_BIN		Suffix to use for executables.
 * MBS_EXT_SHARED	Suffix to use for shared objects.
 * MBS_EXT_STATIC	Suffix to use for static libraries.
 * MBS_DEFAULT_PATH	Default path to search for MBS modules.
 */

/**
 * Windows.
 */
#ifdef _WIN32
#	define MBS_EXT_BIN			".exe"
#	define MBS_EXT_SHARED			".dll"
#	define MBS_EXT_STATIC			".a"
#	define MBS_DEFAULT_PATH			"/mbsroot/lib/mbs"
/**
 * Assume POSIX for everything else.
 */
#else
#	define MBS_EXT_BIN			""
#	define MBS_EXT_SHARED			".so"
#	define MBS_EXT_STATIC			".a"
#	define MBS_DEFAULT_PATH			"/usr/lib/mbs"

#endif	/* platform macros */
#endif	/* SYSVARS_H_ */
