/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _WIN32

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <sys/wait.h>
#include <assert.h>

#include "test.h"

/**
 * ANSI escape sequences to set color.
 */
#define	COLOR_RED		"\033[0;31m"
#define	COLOR_GREEN		"\033[0;32m"
#define COLOR_BLUE		"\033[1;34m"
#define	COLOR_NONE		"\033[0m"

int doUnitTests(char **tests, int count)
{
	int numPassed = 0;
	int numFailed = 0;
	
	FILE *fp = fopen("test.log", "w");
	if (fp  == NULL)
	{
		fprintf(stderr, "mbs: failed to open test.log: %s\n", strerror(errno));
		return 1;
	};
	
	fprintf(fp, "Begin testing.\n");
	int i;
	for (i=0; i<count; i++)
	{
		char *cpath = strdup(tests[i]);
		char *cpath2 = strdup(cpath);
		
		char *dir = dirname(cpath);
		char *base = basename(cpath2);
		
		char buffer[70];
		sprintf(buffer, "Testing `%s'...", base);
		
		fprintf(fp, "Testing `%s'...\n", base);
		printf("%s%-50s %s", COLOR_BLUE, buffer, COLOR_NONE);
		
		fflush(fp);
		fflush(stdout);
		
		pid_t pid = fork();
		if (pid == -1)
		{
			printf("%sFAIL%s\n", COLOR_RED, COLOR_NONE);
			fprintf(fp, "fork: %s\n", strerror(errno));
			fclose(fp);
			return 1;
		}
		else if (pid == 0)
		{
			int fd = fileno(fp);
			close(1);
			close(2);
			assert(dup(fd) != -1);
			assert(dup(fd) != -1);
			close(fd);
			setenv("TESTDIR", dir, 1);
			execl("/bin/sh", "sh", tests[i], NULL);
			_exit(1);
		}
		else
		{
			int status;
			wait(&status);
			
			if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
			{
				fprintf(fp, "TEST PASSED.\n");
				printf("%sPASS%s\n", COLOR_GREEN, COLOR_NONE);
				numPassed++;
			}
			else
			{
				fprintf(fp, "TEST FAILED.\n");
				printf("%sFAIL%s\n", COLOR_RED, COLOR_NONE);
				numFailed++;
			};
		};
		
		free(cpath);
		free(cpath2);
	};
	
	fprintf(fp, "Summary: %d/%d passed, %d/%d failed\n", numPassed, count, numFailed, count);
	fclose(fp);
	
	printf("Summary: %s%d/%d Passed%s, %s%d/%d Failed%s\n",
		COLOR_GREEN, numPassed, count, COLOR_NONE, COLOR_RED, numFailed, count, COLOR_NONE);
	if (numFailed == 0) return 0;
	else return 1;
};

#else /* _WIN32 */
#include <stdio.h>

int doUnitTests(char **tests, int count)
{
	fprintf(stderr, "MBS unit testing is not implemented on Windows yet!\n");
	return 1;
};

#endif /* _WIN32 */
