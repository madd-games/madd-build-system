/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>

#ifndef _WIN32
#include <sys/stat.h>
#else
// seriously windows why
#define	TokenType __NO_GO_AWAY
#include <windows.h>
#undef TokenType
#endif

#include "parse.h"
#include "interp.h"
#include "context.h"
#include "sysvars.h"
#include "test.h"

#ifndef MBS_SYS
#	error Please define MBS_SYS!
#endif

/**
 * Current exit status. This is set to 1 whenever error() is called.
 */
int exitStatus;

Package *firstPackage;
Package *lastPackage;

const char *mbsPath;
const char *mbsSrcDir;
char *mbsTests;

FILE *fpMeta;

void error(const char *filename, int lineno, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	
	fprintf(stderr, "%s:%d: error: ", filename, lineno);
	vfprintf(stderr, format, ap);
	fprintf(stderr, "\n");
	
	va_end(ap);
	exitStatus = 1;
	
	// TODO: tolerate errors ???
	exit(1);
};

int makeDir(const char *path)
{
#ifdef _WIN32
	if (!CreateDirectory(path, NULL))
	{
		switch (GetLastError())
		{
		case ERROR_ALREADY_EXISTS:
			errno = EEXIST;
			break;
		case ERROR_PATH_NOT_FOUND:
			errno = ENOENT;
			break;
		default:
			errno = EAGAIN;
			break;
		};
		
		return -1;
	};
	
	return 0;
#else
	return mkdir(path, 0755);
#endif
};

int main(int argc, char *argv[])
{
	if (argc == 2 && strcmp(argv[1], "--version") == 0)
	{
		fprintf(stderr, "Madd Build System %s\n", MBS_VERSION);
		fprintf(stderr, "Copyright (c) 2018, Madd Games. All rights reserved.\n");
		return 0;
	};
	
	initParser();
	
	char *path = getenv("MBS_PATH");
	if (path == NULL)
	{
		mbsPath = MBS_DEFAULT_PATH;
	}
	else
	{
		mbsPath = path;
	};
	
	mbsTests = strdup("");
	
	if (argc < 2)
	{
		fprintf(stderr, "mbs: no script specified\n");
		return 1;
	};
	
	if (strcmp(argv[1], "--test") == 0)
	{
		return doUnitTests(&argv[2], argc-2);
	};
	
	const char *script = argv[1];
	FILE *fp = fopen(script, "r");
	if (fp == NULL)
	{
		fprintf(stderr, "mbs: cannot open `%s`: %s\n", script, strerror(errno));
		return 1;
	};
	
	Token *tokens = parseFile(script, fp);
	fclose(fp);
	
	// set up the global context
	pushContext();
	char *sdata = strdup(script);
	mbsSrcDir = strdup(dirname(sdata));
	setvar("mbs_srcdir", mbsSrcDir);
	free(sdata);
	
	setvar("mbs_ext_bin", MBS_EXT_BIN);
	setvar("mbs_ext_shared", MBS_EXT_SHARED);
	setvar("mbs_ext_static", MBS_EXT_STATIC);
	setvar("mbs_sys", MBS_SYS);
	
	char *mbsReleaseVersion = getenv("MBS_RELEASE_VERSION");
	if (mbsReleaseVersion != NULL)
	{
		setvar("mbs_release_version", mbsReleaseVersion);
	}
	else
	{
		setvar("mbs_release_version", "INTERNAL");
	};
	
	char *mbsMergeEnv = getenv("MBS_MERGE");
	const char *mbsMerge;
	if (mbsMergeEnv == NULL)
	{
		setvar("mbs_merge", "mbs-merge");
		mbsMerge = "mbs-merge";
	}
	else
	{
		setvar("mbs_merge", mbsMergeEnv);
		mbsMerge = strdup(mbsMergeEnv);
	};
	
	int i;
	for (i=2; i<argc; i++)
	{
		const char *arg = argv[i];
		if (memcmp(arg, "--", 2) == 0)
		{
			char *buffer = (char*) malloc(strlen(arg) - 2 + 4 + 1);
			sprintf(buffer, "opt_%s", &arg[2]);
			
			const char *optname = buffer;
			const char *value = "";
			
			char *equals = strchr(buffer, '=');
			if (equals != NULL)
			{
				*equals = 0;
				value = equals+1;
			};
			
			setvar(optname, value);
		}
		else
		{
			fprintf(stderr, "mbs: unrecognized command-line option `%s'\n", arg);
			return 1;
		};
	};
	
	fpMeta = fopen("mbs.meta", "w");
	if (fpMeta == NULL)
	{
		fprintf(stderr, "ERROR: Failed to create mbs.meta: %s\n", strerror(errno));
		return 1;
	};
	
	interpret(tokens);
	fclose(fpMeta);
	
	if (exitStatus == 0)
	{
		FILE *fpMake = fopen("Makefile", "r");
		if (fpMake != NULL)
		{
			fclose(fpMake);
			if (system("make clean") != 0)
			{
				exit(1);
			};
			
			remove("Makefile");
		};
		
		if (makeDir("obj") != 0)
		{
			fprintf(stderr, "mbs: could not create directory `obj': %s\n", strerror(errno));
			return 1;
		};
		
		fprintf(stderr, "Generating a Makefile...\n");
		
		FILE *fp = fopen("Makefile", "w");
		if (fp == NULL)
		{
			fprintf(stderr, "ERROR: Cannot open Makefile: %s\n", strerror(errno));
			return 1;
		};
		
		fprintf(fp, "# Makefile generated by MBS; do not edit manually!\n");
		fprintf(fp, ".PHONY: all install clean distclean test ");
		
		Package *pkg;
		for (pkg=firstPackage; pkg!=NULL; pkg=pkg->next)
		{
			fprintf(fp, "%s install-%s ", pkg->name, pkg->name);
		};
		
		fprintf(fp, "\n");
		
		fprintf(fp, "all: ");
		for (pkg=firstPackage; pkg!=NULL; pkg=pkg->next)
		{
			fprintf(fp, "%s ", pkg->name);
		};
		fprintf(fp, "\n");

		fprintf(fp, "install: ");
		for (pkg=firstPackage; pkg!=NULL; pkg=pkg->next)
		{
			fprintf(fp, "install-%s ", pkg->name);
		};
		fprintf(fp, "\n");

		for (pkg=firstPackage; pkg!=NULL; pkg=pkg->next)
		{
			if (makeDir(pkg->name) != 0)
			{
				fprintf(stderr, "mbs: failed to create directory `%s': %s\n",
					pkg->name, strerror(errno));
				return 1;
			};
			fprintf(fp, "%s: ", pkg->name);
			
			FileDef *file;
			for (file=pkg->first; file!=NULL; file=file->next)
			{
				fprintf(fp, "%s/%s ", pkg->name, file->name);
			};
			
			fprintf(fp, "\n");
			
			for (file=pkg->first; file!=NULL; file=file->next)
			{
				MakeRule *rule;
				for (rule=file->first; rule!=NULL; rule=rule->next)
				{
					fprintf(fp, "%s\n", rule->rulehead);
					
					size_t i;
					for (i=0; i<rule->numCmds; i++)
					{
						fprintf(fp, "\t%s\n", rule->cmds[i]);
					};
				};
			};
			
			fprintf(fp, "install-%s:\n", pkg->name);
			fprintf(fp, "\t%s %s $(DESTDIR)/\n", mbsMerge, pkg->name);
		};
		
		fprintf(fp, "clean:\n");
		fprintf(fp, "\trm -rf obj ");
		
		for (pkg=firstPackage; pkg!=NULL; pkg=pkg->next)
		{
			fprintf(fp, "%s ", pkg->name);
		};
		
		fprintf(fp, "\n");

		fprintf(fp, "distclean: clean\n");
		fprintf(fp, "\trm Makefile mbs.meta\n");
		
		fprintf(fp, "test:\n");
		fprintf(fp, "\tmbs --test %s\n", mbsTests);
		
		fclose(fp);
	};
		
	return exitStatus;
};
