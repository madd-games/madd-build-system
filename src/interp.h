/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef INTERP_H
#define INTERP_H_

#include "lex.h"
#include "parse.h"

/**
 * Make rule.
 */
typedef struct MakeRule_
{
	/**
	 * Link.
	 */
	struct MakeRule_ *next;
	
	/**
	 * Rule definition line.
	 */
	char *rulehead;
	
	/**
	 * Command lines (these must be prepended with TABs when writing to the Makefile).
	 */
	char **cmds;
	size_t numCmds;
} MakeRule;

/**
 * File definition.
 */
typedef struct FileDef_
{
	/**
	 * Link.
	 */
	struct FileDef_ *next;
	
	/**
	 * Name of the file (within the package).
	 */
	char *name;
	
	/**
	 * List of make rules.
	 */
	MakeRule *first;
	MakeRule *last;
} FileDef;

/**
 * Package definition.
 */
typedef struct Package_
{
	/**
	 * Link.
	 */
	struct Package_ *next;
	
	/**
	 * Name and version.
	 * Version might be an empty string.
	 */
	const char *name;
	const char *version;
	
	/**
	 * Description. May be an empty string.
	 */
	const char *desc;
	
	/**
	 * Debian dependencies. "libc6" is always here.
	 */
	const char *debdep;
	
	/**
	 * Package developer. Initally set to "unknown@example.com".
	 */
	const char *dever;
	
	/**
	 * List of files.
	 */
	FileDef *first;
	FileDef *last;
} Package;

/**
 * Macro definition.
 */
typedef struct Macro_
{
	/**
	 * Link.
	 */
	struct Macro_ *next;
	
	/**
	 * Macro name.
	 */
	const char *name;
	
	/**
	 * List of argument names, terminated by a NULL pointer.
	 */
	const char **args;
	
	/**
	 * List of tokens defining the macro.
	 */
	Token *def;
} Macro;

/**
 * Interpret a stream of tokens, and generate an internal definition of build rules.
 * This is used before generating a Makefile.
 */
void interpret(Token *tokens);

#endif
