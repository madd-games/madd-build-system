/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "context.h"

static Context *currentContext;

void pushContext()
{
	Context *ctx = (Context*) malloc(sizeof(Context));
	ctx->up = currentContext;
	currentContext = ctx;
	
	ctx->exportTarget = 0;
	ctx->first = ctx->last = NULL;
};

void pushExportContext()
{
	pushContext();
	currentContext->exportTarget = 1;
};

void popContext()
{
	Context *ctx = currentContext;
	currentContext = ctx->up;
	
	while (ctx->first != NULL)
	{
		VarSetting *var = ctx->first;
		ctx->first = var->next;
		
		free(var->name);
		free(var->value);
		free(var);
	};
	
	free(ctx);
};

char* getvar(const char *name)
{
	if (strcmp(name, "mbs_genid") == 0)
	{
		static int num = 0;
		
		char *buffer = (char*) malloc(16);
		sprintf(buffer, "%d", num++);
		return buffer;
	};
	
	Context *ctx;
	for (ctx=currentContext; ctx!=NULL; ctx=ctx->up)
	{
		VarSetting *var;
		for (var=ctx->first; var!=NULL; var=var->next)
		{
			if (strcmp(var->name, name) == 0)
			{
				return var->value;
			};
		};
	};
	
	return NULL;
};

void setvar(const char *name, const char *value)
{
	VarSetting *var;
	for (var=currentContext->first; var!=NULL; var=var->next)
	{
		if (strcmp(var->name, name) == 0)
		{
			free(var->value);
			var->value = strdup(value);
			return;
		};
	};
	
	var = (VarSetting*) malloc(sizeof(VarSetting));
	var->next = NULL;
	var->name = strdup(name);
	var->value = strdup(value);
	
	if (currentContext->last == NULL)
	{
		currentContext->first = currentContext->last = var;
	}
	else
	{
		currentContext->last->next = var;
		currentContext->last = var;
	};
};

int expvar(const char *name)
{
	if (currentContext->up == NULL) return 0;
	
	const char *value = getvar(name);
	if (value == NULL)
	{
		return -1;
	};
	
	Context *save = currentContext;
	currentContext = save->up;
	while (currentContext->up != NULL && !currentContext->exportTarget) currentContext = currentContext->up;
	
	setvar(name, value);
	currentContext = save;
	
	return 0;
};
