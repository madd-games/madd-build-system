/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <libgen.h>

#include "interp.h"
#include "main.h"
#include "context.h"

extern Package *firstPackage;
extern Package *lastPackage;

static const char **importList;
static size_t numImports;

static Macro *macroHead;
static FileDef *currentFile;

static Token* parseFileSpec(Token *tok, FileDef *file);

extern FILE* fpMeta;	// main.c

static char *parseStringLiteral(const char *tokval)
{
	char *result = (char*) malloc(strlen(tokval)+1);
	if (result == NULL)
	{
		fprintf(stderr, "mbs: out of memory\n");
		abort();
	};
	
	char *put = result;
	const char *end = &tokval[strlen(tokval)-1];
	
	tokval++;
	while (tokval != end)
	{
		if (*tokval == '\\')
		{
			*put++ = *++tokval;
		}
		else
		{
			*put++ = *tokval;
		};
		
		tokval++;
	};
	
	*put = 0;
	return result;
};

static Token* parseString(Token *tok, char **out)
{	
	if (tok->type == TOK_STRING)
	{
		*out = parseStringLiteral(tok->value);
		return tok->next;
	}
	else if (strcmp(tok->value, "@") == 0)
	{
		char *cmd;
		
		tok = tok->next;
		tok = parseString(tok, &cmd);
		
		FILE *fp = popen(cmd, "r");
		free(cmd);
		
		if (fp == NULL)
		{
			*out = "<popen failed>";
			return tok;
		};
		
		char buffer[4096];
		char *put = buffer;
		int spaceLeft = 4095;
		
		while (1)
		{
			int count = fread(put, 1, spaceLeft, fp);
			if (count == 0) break;
			put += count;
		};
		
		*put = 0;
		pclose(fp);
		
		char *endline = strchr(buffer, '\n');
		if (endline != NULL) *endline = 0;
		
		*out = strdup(buffer);
		return tok;
	}
	else if (strcmp(tok->value, "(") == 0)
	{
		char *result = (char*) malloc(1);
		result[0] = 0;
		
		tok = tok->next;
		while (strcmp(tok->value, ")") != 0)
		{
			if (tok->type == TOK_STRING)
			{
				char *part = parseStringLiteral(tok->value);
				char *newstr = (char*) malloc(strlen(result) + strlen(part) + 1);
				sprintf(newstr, "%s%s", result, part);
				free(part);
				free(result);
				result = newstr;
				tok = tok->next;
			}
			else if (tok->type == TOK_ID)
			{
				char *part = getvar(tok->value);
				if (part == NULL)
				{
					error(tok->filename, tok->lineno, "undefined variable `%s'", tok->value);
					part = strdup("");
				};
				
				char *newstr = (char*) malloc(strlen(result) + strlen(part) + 1);
				sprintf(newstr, "%s%s", result, part);
				free(result);
				result = newstr;
				tok = tok->next;
			}
			else
			{
				error(tok->filename, tok->lineno, "expected a string literal or identifier, not `%s'", tok->value);
				break;
			};
		};
		
		*out = result;
		return tok->next;
	}
	else
	{
		error(tok->filename, tok->lineno, "expected string, not `%s'", tok->value);
		*out = "?";
		return tok->next;
	};
};

static int patternMatch(const char *subject, const char *pattern)
{
	while (*pattern != 0)
	{
		if (*pattern == '?')
		{
			if (*subject == 0) return 1;
			
			subject++;
			pattern++;
		}
		else if (*pattern == '*')
		{
			const char *sub;
			for (sub=subject; *sub!=0; sub++)
			{
				if (patternMatch(sub, pattern+1) == 0) return 0;
			};
			
			if (patternMatch("", pattern+1) == 0) return 0;
			return 1;
		}
		else
		{
			if (*subject++ != *pattern++) return 1;
		};
	};
	
	if (*subject == 0) return 0;
	else return 1;
};

static Token* parseSwitch(Token *tok, const char *subject);

static Token* parseStatement(Token *tok)
{
	if (strcmp(tok->value, "set") == 0)
	{
		tok = tok->next;
		if (tok->type != TOK_ID)
		{
			error(tok->filename, tok->lineno, "invalid syntax: expecting identifier for name after `set'");
			return tok;
		};
		
		const char *varname = tok->value;
		tok = tok->next;
		
		int overwrite;
		if (strcmp(tok->value, "=") == 0)
		{
			overwrite = 1;
		}
		else if (strcmp(tok->value, "?=") == 0)
		{
			overwrite = 0;
		}
		else
		{
			error(tok->filename, tok->lineno, "invalid syntax: expecting `=' or `?=' not `%s'", tok->value);
			overwrite = 0;
		};
		tok = tok->next;
		
		char *value;
		tok = parseString(tok, &value);
		
		char *oldval = getvar(varname);
		if (oldval == NULL || overwrite)
		{
			setvar(varname, value);
		};
		
		return tok;
	}
	else if (strcmp(tok->value, "unit_test") == 0)
	{
		tok = tok->next;
		char *path;
		tok = parseString(tok, &path);
		
		char *newtests = (char*) malloc(strlen(mbsTests) + strlen(path) + 2);
		sprintf(newtests, "%s %s", mbsTests, path);
		
		free(path);
		free(mbsTests);
		mbsTests = newtests;
		
		return tok;
	}
	else if (strcmp(tok->value, "error") == 0)
	{
		exit(1);
	}
	else if (strcmp(tok->value, "switch") == 0)
	{
		tok = tok->next;
		
		char *subject;
		tok = parseString(tok, &subject);
		
		if (strcmp(tok->value, "{") != 0)
		{
			error(tok->filename, tok->lineno, "expected `{' not `%s'", tok->value);
			return tok;
		};
		
		tok = tok->next;
		tok = parseSwitch(tok, subject);
		return tok;
	}
	else if (strcmp(tok->value, "print") == 0)
	{
		tok = tok->next;
		
		char *msg;
		tok = parseString(tok, &msg);
		
		printf("%s\n", msg);
		return tok;
	}
	else if (strcmp(tok->value, "meta") == 0)
	{
		tok = tok->next;
		
		char *key;
		tok = parseString(tok, &key);
		
		char *value;
		tok = parseString(tok, &value);
		
		fprintf(fpMeta, "%s %s\n", key, value);
		return tok;
	}
	else if (strcmp(tok->value, "export") == 0)
	{
		tok = tok->next;
		
		if (tok->type != TOK_ID)
		{
			error(tok->filename, tok->lineno, "expected variable name after `export'");
			return tok->next;
		};
		
		if (expvar(tok->value) != 0)
		{
			error(tok->filename, tok->lineno, "undefined variable `%s'", tok->value);
			return tok->next;
		};
		
		return tok->next;
	}
	else if (tok->type == TOK_ID && currentFile != NULL)
	{
		FileDef *file = currentFile;
		
		Macro *mac;
		for (mac=macroHead; mac!=NULL; mac=mac->next)
		{
			if (strcmp(mac->name, tok->value) == 0)
			{
				break;
			};
		};
		
		if (mac == NULL)
		{
			error(tok->filename, tok->lineno, "undefined macro `%s'", tok->value);
			return tok;
		};
		
		tok = tok->next;
		
		pushContext();
		const char **scan = mac->args;
		while (*scan != NULL)
		{
			const char *argname = *scan++;
			char *value;
			
			tok = parseString(tok, &value);
			setvar(argname, value);
		};
		parseFileSpec(mac->def, file);
		popContext();
		
		return tok;
	}
	else if (strcmp(tok->value, "makerule") == 0 && currentFile != NULL)
	{
		FileDef *file = currentFile;
		tok = tok->next;
		
		MakeRule *rule = (MakeRule*) malloc(sizeof(MakeRule));
		rule->next = NULL;
		tok = parseString(tok, &rule->rulehead);
		rule->cmds = NULL;
		rule->numCmds = 0;
		
		while (strcmp(tok->value, ";") != 0)
		{
			size_t index = rule->numCmds++;
			rule->cmds = (char**) realloc(rule->cmds, sizeof(void*) * rule->numCmds);
			tok = parseString(tok, &rule->cmds[index]);
		};
		
		if (file->last == NULL)
		{
			file->first = file->last = rule;
		}
		else
		{
			file->last->next = rule;
			file->last = rule;
		};
		
		return tok;
	}
	else if (strcmp(tok->value, ";") == 0)
	{
		return tok->next;
	}
	else
	{
		return NULL;
	};
};

static Token* parseSwitch(Token *tok, const char *subject)
{
	Token *endTok = tok;
	int depth = 0;
	
	while (strcmp(endTok->value, "}") != 0 || depth != 0)
	{
		if (endTok->next == endTok)
		{
			error(endTok->filename, endTok->lineno, "expected `}' before `%s'", endTok->value);
			return endTok;
		};
		
		if (strcmp(endTok->value, "{") == 0)
		{
			depth++;
		}
		else if (strcmp(endTok->value, "}") == 0)
		{
			depth--;
		};
		
		endTok = endTok->next;
	};
	
	if (strcmp(tok->value, "case") != 0)
	{
		error(tok->filename, tok->lineno, "the body of a switch statement must begin with a `case'");
	};
	
	while (1)
	{
		while (strcmp(tok->value, "case") != 0 && tok != endTok)
		{
			tok = tok->next;
		};
		
		if (tok == endTok) break;
		
		// OK, found a case statement
		tok = tok->next;
		
		if (tok == endTok)
		{
			error(tok->filename, tok->lineno, "expected a string after `case'");
			break;
		};
		
		char *pattern;
		tok = parseString(tok, &pattern);
		
		// if there is a pattern match, then execute from here by breaking;
		// else, continue the search
		if (patternMatch(subject, pattern) == 0)
		{
			break;
		};
	};
	
	while (tok != endTok)
	{
		Token *nextTok;
		
		if (strcmp(tok->value, "break") == 0)
		{
			// nice
			break;
		}
		else if (strcmp(tok->value, "case") == 0)
		{
			tok = tok->next;
			
			if (tok == endTok)
			{
				error(tok->filename, tok->lineno, "expected a string after `case'");
				break;
			};
			
			char *pattern;
			tok = parseString(tok, &pattern);
		}
		else if ((nextTok = parseStatement(tok)) != NULL)
		{
			tok = nextTok;
		}
		else
		{
			error(tok->filename, tok->lineno, "unexpected `%s'", tok->value);
			break;
		};
	};
	
	return endTok->next;
};

#if 0
static Token* parseFileSpecStatement(Token *tok, FileDef *file)
{
		Token *nextTok;
		
		if (tok->type == TOK_END)
		{
			error(tok->filename, tok->lineno, "expected `}' before EOF");
			break;
		}

		else if (strcmp(tok->value, ";") == 0)
		{
			tok = tok->next;
		}
		else if ((nextTok = parseStatement(tok)) != NULL)
		{
			tok = nextTok;
		}
		else
		{
			error(tok->filename, tok->lineno, "unexpected `%s'", tok->value);
			break;
		};
};
#endif

static Token* parseFileSpec(Token *tok, FileDef *file)
{
	FileDef *oldFile = currentFile;
	currentFile = file;
	
	while (strcmp(tok->value, "}") != 0)
	{
		tok = parseStatement(tok);
		if (tok == NULL) break;
	};
	
	currentFile = oldFile;
	return tok->next;
};

static Token* parsePackage(Token *tok, Package *pkg)
{
	while (strcmp(tok->value, "}") != 0)
	{
		Token *nextTok;
		
		if (tok->type == TOK_END)
		{
			error(tok->filename, tok->lineno, "expected `}' before EOF");
			break;
		}
		else if (strcmp(tok->value, "file") == 0)
		{
			tok = tok->next;
			
			char *filename;
			tok = parseString(tok, &filename);
			
			if (strcmp(tok->value, "{") != 0)
			{
				error(tok->filename, tok->lineno, "expected `{' not `%s'", tok->value);
				return tok;
			};
			
			tok = tok->next;
			
			FileDef *file = (FileDef*) malloc(sizeof(FileDef));
			file->next = NULL;
			file->name = strdup(filename);
			file->first = file->last = NULL;
			
			if (pkg->last == NULL)
			{
				pkg->first = pkg->last = file;
			}
			else
			{
				pkg->last->next = file;
				pkg->last = file;
			};
			
			char *realFileName = (char*) malloc(strlen(pkg->name) + strlen(filename) + 2);
			sprintf(realFileName, "%s/%s", pkg->name, filename);
			free(filename);
			
			pushExportContext();
			setvar("mbs_file", realFileName);
			setvar("mbs_dir", dirname(realFileName));
			free(realFileName);
			tok = parseFileSpec(tok, file);
			popContext();
		}
		else if (strcmp(tok->value, ";") == 0)
		{
			tok = tok->next;
		}
		else if ((nextTok = parseStatement(tok)) != NULL)
		{
			tok = nextTok;
		}
		else
		{
			error(tok->filename, tok->lineno, "unexpected `%s'", tok->value);
			break;
		};
	};
	
	return tok->next;
};

static Token* getTokenList(Token *tok, Token **outlist)
{
	Token *firstToken = NULL;
	Token *lastToken = NULL;
	int depth = 0;
	
	while (strcmp(tok->value, "}") != 0 || depth != 0)
	{
		if (tok->type == TOK_END)
		{
			error(tok->filename, tok->lineno, "expected `}' before EOF");
			return tok;
		};
		
		if (strcmp(tok->value, "{") == 0)
		{
			depth++;
		}
		else if (strcmp(tok->value, "}") == 0)
		{
			depth--;
		};
		
		Token *newtok = (Token*) malloc(sizeof(Token));
		memcpy(newtok, tok, sizeof(Token));
		newtok->next = NULL;
		
		if (lastToken == NULL)
		{
			firstToken = lastToken = newtok;
		}
		else
		{
			lastToken->next = newtok;
			lastToken = newtok;
		};
		
		tok = tok->next;
	};
	
	Token *end = (Token*) malloc(sizeof(Token));
	end->next = end;
	end->type = TOK_OP;
	end->value = "}";
	end->filename = tok->filename;
	end->lineno = tok->lineno;
	
	if (lastToken == NULL)
	{
		firstToken = lastToken = end;
	}
	else
	{
		lastToken->next = end;
	};
	
	*outlist = firstToken;
	return tok->next;
};

void interpret(Token *tok)
{
	while (tok->type != TOK_END)
	{
		Token *nextTok;
		
		if (strcmp(tok->value, ";") == 0)
		{
			tok = tok->next;
		}
		else if (strcmp(tok->value, "import") == 0)
		{
			Token *importTok = tok;
			
			tok = tok->next;
			
			if (tok->type != TOK_ID)
			{
				error(tok->filename, tok->lineno, "invalid syntax: expecting module name after `import'");
				break;
			};
			
			const char *modname = tok->value;
			tok = tok->next;
			
			int found = 0;
			size_t i;
			for (i=0; i<numImports; i++)
			{
				if (strcmp(importList[i], modname) == 0)
				{
					// already imported
					found = 1;
					break;
				};
			};

			if (found) continue;
			
			i = numImports++;
			importList = (const char**) realloc(importList, sizeof(void*) * numImports);
			importList[i] = modname;
			
			// allocate a buffer large enough for anything
			FILE *fp;
			char *pathbuf = (char*) malloc(strlen(mbsPath) + strlen(mbsSrcDir) + strlen(modname) + 16);
			
			// option 1: it's in the source directory
			sprintf(pathbuf, "%s/%s.mbs", mbsSrcDir, modname);
			fp = fopen(pathbuf, "r");
			if (fp == NULL)
			{
				// option 2: it's in subdirectory called "mbs" in the source directory
				sprintf(pathbuf, "%s/mbs/%s.mbs", mbsSrcDir, modname);
				fp = fopen(pathbuf, "r");
				if (fp == NULL)
				{
					// option 3: it's in the standard library directory
					sprintf(pathbuf, "%s/%s.mbs", mbsPath, modname);
					fp = fopen(pathbuf, "r");
					if (fp == NULL)
					{
						error(importTok->filename, importTok->lineno,
							"could not find a module named `%s'", modname);
						continue;
					};		
				};
			};
			
			Token *strm = parseFile(pathbuf, fp);
			free(pathbuf);
			
			interpret(strm);
		}
		else if (strcmp(tok->value, "package") == 0)
		{
			tok = tok->next;
			
			char *pkgname;
			tok = parseString(tok, &pkgname);
			char *pkgversion;
			tok = parseString(tok, &pkgversion);
			
			fprintf(fpMeta, "mbs.pkg %s\n", pkgname);
			fprintf(fpMeta, "mbs.pkg.version %s %s\n", pkgname, pkgversion);
			
			if (strcmp(tok->value, "{") != 0)
			{
				error(tok->filename, tok->lineno, "invalid syntax: expecting package definition (starting with `{')");
				break;
			};
			
			tok = tok->next;
			
			Package *pkg = (Package*) malloc(sizeof(Package));
			if (pkg == NULL)
			{
				fprintf(stderr, "mbs: out of memory\n");
				exit(1);
			};
			
			pkg->next = NULL;
			pkg->name = pkgname;
			pkg->version = pkgversion;
			pkg->desc = "";
			pkg->debdep = "libc6 (>= 2.1)";
			pkg->dever = "unknown@example.com";
			pkg->first = pkg->last = NULL;
			
			pushContext();
			tok = parsePackage(tok, pkg);
			popContext();
			
			if (lastPackage == NULL)
			{
				firstPackage = lastPackage = pkg;
			}
			else
			{
				lastPackage->next = pkg;
				lastPackage = pkg;
			};
		}
		else if (strcmp(tok->value, "macro") == 0)
		{
			tok = tok->next;
			
			if (tok->type != TOK_ID)
			{
				error(tok->filename, tok->lineno, "expected macro name after `macro'");
				break;
			};
			
			Macro *macro = (Macro*) malloc(sizeof(Macro));
			macro->next = NULL;
			macro->name = tok->value;
			macro->args = NULL;
			
			size_t numArgs = 0;
			tok = tok->next;
			
			while (tok->type == TOK_ID)
			{
				size_t i = numArgs++;
				macro->args = (const char**) realloc(macro->args, sizeof(void*) * numArgs);
				macro->args[i] = tok->value;
				tok = tok->next;
			};
			
			size_t i = numArgs++;
			macro->args = (const char**) realloc(macro->args, sizeof(void*) * numArgs);
			macro->args[i] = NULL;
			
			if (strcmp(tok->value, "{") != 0)
			{
				error(tok->filename, tok->lineno, "expected `{' not `%s'", tok->value);
				break;
			};
			
			tok = tok->next;
			tok = getTokenList(tok, &macro->def);
			
			macro->next = macroHead;
			macroHead = macro;
		}
		else if ((nextTok = parseStatement(tok)) != NULL)
		{
			tok = nextTok;
		}
		else
		{
			error(tok->filename, tok->lineno, "invalid syntax: unexpected token `%s'", tok->value);
			break;
		};
	};
};
