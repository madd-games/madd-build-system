/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef CONTEXT_H_
#define CONTEXT_H_

/**
 * Variable setting.
 */
typedef struct VarSetting_
{
	/**
	 * Link.
	 */
	struct VarSetting_ *next;
	
	/**
	 * Variable name.
	 */
	char *name;
	
	/**
	 * Variable value.
	 */
	char *value;
} VarSetting;

/**
 * Context. This is a list of variables defined in a specific scope, plus a link to the superscope.
 */
typedef struct Context_
{
	/**
	 * Pointer to the supercontext.
	 */
	struct Context_ *up;
	
	/**
	 * Variable setting list.
	 */
	VarSetting *first;
	VarSetting *last;
	
	/**
	 * Set to 1 if variables are to be exported to this context.
	 */
	int exportTarget;
} Context;

/**
 * Create a new context and push to the top of the stack.
 */
void pushContext();

/**
 * Create a new context and push to the top of the stack, and mark it as an export target.
 */
void pushExportContext();

/**
 * Pop a context. This destroys the current context, removing all variables from it, and returning to
 * the previous context.
 */
void popContext();

/**
 * Return the value of a variable. If it does not exist, then NULL is returned.
 */
char* getvar(const char *name);

/**
 * Set a variable in the current context. If the variable does not exist in the current context, it is
 * created; this means that setting variables in a context shadows the variables from supercontexts.
 */
void setvar(const char *name, const char *value);

/**
 * Export a variable. This moves it to the first export target above.Returns -1 if the variable does not exist;
 * 0 on success.
 */
int expvar(const char *name);

#endif
