/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parse.h"
#include "main.h"

/**
 * List of primary token types.
 */
static TokenDef tokenDefs[] = {
	// whitespace: all whitespace characters and comments
	{TOK_WHITESPACE, "$+"},
	{TOK_WHITESPACE, "\\/\\/.*?\n"},
	{TOK_WHITESPACE, "\\/\\*.*?(\\*\\/)"},
	{TOK_WHITESPACE, "#.*?\n"},
	
	// identifiers
	{TOK_ID, "[_a-zA-Z][_a-zA-Z0-9]*"},
	
	// operators
	{TOK_OP, "(\\;|\\{|\\}|\\(|\\)|\\=|\\?\\=|\\@)"},
	
	// string
	{TOK_STRING, "\"(\\\\.|.)*?\""},
	{TOK_STRING, "'(\\\\.|.)*?'"},
	
	// LIST TERMINATOR
	{-1}
};

/**
 * List of keywords: identifiers which are re-classified as TOK_KEYWORD.
 */
static const char *keywords[] = {
	"set",
	"package",
	"file",
	"makerule",
	"print",
	"import",
	"export",
	"macro",
	"switch",
	"case",
	"break",
	"error",
	"unit_test",
	"meta",
	
	// LIST TERMINATOR
	NULL
};

/**
 * Names of the token types.
 */
const char *tokenTypeNames[] = {
	"<whitespace>",
	"<identifier>",
	"<operator>",
	"<keyword>",
	"<string>",
	"<eof>",
};

void initParser()
{
	// compile the expressions
	TokenDef *def;
	for (def=tokenDefs; def->type!=-1; def++)
	{
		def->regex = lexCompileRegex(def->spec);
		if (def->regex == NULL)
		{
			fprintf(stderr, "bad regex: `%s`\n", def->spec);
			abort();
		};
	};
};

Token* parseFile(const char *filename, FILE *fp)
{
	filename = strdup(filename);
	if (filename == NULL)
	{
		fprintf(stderr, "mbs: out of memory\n");
		abort();
	};
	
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	char *data = (char*) malloc(fsize+1);
	if (data == NULL)
	{
		fprintf(stderr, "mbs: out of memory\n");
		abort();
	};
	
	fsize = fread(data, 1, fsize, fp);
	data[fsize] = 0;
	
	Token *firstToken = NULL;
	Token *lastToken = NULL;
	int lineno = 1;
	
	const char *scan = data;
	while (*scan != 0)
	{
		TokenDef *def;
		ssize_t match = -1;
		
		for (def=tokenDefs; def->type!=-1; def++)
		{
			match = lexMatch(def->regex, scan);
			if (match != -1)
			{
				break;
			};
		};
		
		if (match < 1)
		{
			error(filename, lineno, "unexpected character in input: `%c`\n", *scan);
			exit(1);
		};
		
		Token *tok = (Token*) malloc(sizeof(Token));
		if (tok == NULL)
		{
			fprintf(stderr, "mbs: out of memory\n");
			abort();
		};
		
		char *val = (char*) malloc(match+1);
		if (val == NULL)
		{
			fprintf(stderr, "mbs: out of memory\n");
			abort();
		};
		
		memcpy(val, scan, match);
		val[match] = 0;
		
		tok->next = NULL;
		tok->type = def->type;
		tok->value = val;
		tok->filename = filename;
		tok->lineno = lineno;
		
		if (tok->type == TOK_ID)
		{
			const char **kw;
			for (kw=keywords; *kw!=NULL; kw++)
			{
				if (strcmp(*kw, tok->value) == 0)
				{
					tok->type = TOK_KEYWORD;
					break;
				};
			};
		};
		
		while (match--)
		{
			if (*scan++ == '\n') lineno++;
		};
		
		if (tok->type == TOK_WHITESPACE)
		{
			free(val);
			free(tok);
		}
		else if (lastToken == NULL)
		{
			firstToken = lastToken = tok;
		}
		else
		{
			lastToken->next = tok;
			lastToken = tok;
		};
	};
	
	Token *end = (Token*) malloc(sizeof(Token));
	end->next = end;
	end->type = TOK_END;
	end->value = "";
	end->filename = filename;
	end->lineno = lineno;
	
	if (lastToken == NULL)
	{
		return end;
	};
	
	lastToken->next = end;
	return firstToken;
};
