/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PARSE_H_
#define PARSE_H_

#include <stdio.h>

#include "lex.h"

/**
 * Enumeration of token types.
 */
typedef enum
{
	/**
	 * Leave TOK_WHITESPACE at the top to ensure its value is 0.
	 */
	TOK_WHITESPACE,			/* whitespace */
	TOK_END,			/* end of stream */
	
	TOK_ID,				/* identifier */
	TOK_OP,				/* operator */
	TOK_KEYWORD,			/* keyword */
	TOK_STRING,			/* string */
} TokenType;

/**
 * Map token types to names.
 */
extern const char *tokenTypeNames[];

/**
 * Defines a token type.
 */
typedef struct
{
	/**
	 * Type of token.
	 */
	TokenType type;
	
	/**
	 * Regular expression specifier for this type of token.
	 */
	const char *spec;
	
	/**
	 * Compiled expression.
	 */
	Regex *regex;
} TokenDef;

/**
 * Describes a token extracted from input.
 */
typedef struct Token_
{
	/**
	 * Next in the stream.
	 */
	struct Token_ *next;
	
	/**
	 * Type of token.
	 */
	TokenType type;
	
	/**
	 * Token value (the extracted string matching the token pattern).
	 */
	const char *value;
	
	/**
	 * Source file and line number.
	 */
	const char *filename;
	int lineno;
} Token;

/**
 * Initialize the parser.
 */
void initParser();

/**
 * Convert an input file into a stream of tokens. Return the head of the stream on success,
 * or print messages and abort on error. If NULL is returned, then the file is whitespace only,
 * but nethertheless valid.
 */
Token* parseFile(const char *filename, FILE *fp);

#endif
