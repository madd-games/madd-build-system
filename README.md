# Madd Build System

A build system primarily targetting Unix-like systems but more sane than the GNU build system and also it can handle packaging.

## Building

If you already have an older version of MBS installed, you can configure MBS using the following command (please run it outside of the main package directory):

```
../madd-build-system/configure
```

If you do not yet have MBS installed, you need to run the 'bootstrap' script:

```
../madd-build-system/bootstrap.sh
```

This script builds a temporary MBS binary, and then runs `configure` using its own library, and thus does not depend on any MBS installation. The final result is the same as running `configure`.

Once configured, you can build by simply running `make`. To install, run `make install`.
