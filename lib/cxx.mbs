/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * MODULE `cxx'
 *
 * Macros for compiling C++ projects.
 *
 * This module complements the module for C (`cc'). There is a cxx_* macro for each cc_* macro and they
 * interoperate: most of the cxx_* macros simply call the equivalent cc_* macro; the point of this is to
 * allow C modules to be imported into C++ projects.
 *
 * Certain macros function differently: cxx_host, cxx_srcdir and cxx_link. You must use cxx_host after cc_host,
 * to set the correct C++ compiler. C++ source directories must be added using cxx_srcdir, so that they use the
 * C++ compiler. Finally, cxx_link must be used to create the final executable/shared library (however, you can use
 * either cc_shared or cxx_shared to mark it as a shared library). cxx_link will link both C++ as well as plain
 * C sources (added with cc_srcdir).
 *
 * As a rule of thumb: use cxx_* if you KNOW you are targetting C++, but continue freely using macros for importing
 * C libraries, as if it was just C.
 */

import stdopt;
import hostconf;
import cc;

/**
 * cxx_host <host>
 *
 * Change the host system. This decides which compiler should be used. By default, it's the
 * selected host system.
 */
macro cxx_host host
{
	switch (host)
	{
		case (build)
			set cxx_compiler = 'g++';
			break;
		case '*'
			set cxx_compiler = (host '-g++');
			break;
	};
	
	export cxx_compiler;
};

switch (host)
{
	case (build)
		set cxx_compiler = 'g++';
		break;
	case '*'
		set cxx_compiler = (host '-g++');
		break;
};

print ('C++ compiler: ' cxx_compiler);

/**
 * cxx_srcdir <directory>
 *
 * Build a binary from C++ files in the specified directory (and its subdirectories).
 */
macro cxx_srcdir dirname
{
	// add to the meta file
	meta 'cxx.srcdir' (dirname);
	
	// set up parameters
	set cc_cflags_define ?= '';
	set cc_cflags_extra ?= '';
	set cc_cflags ?= (cc_cflags_define ' -Wall -Werror ' cc_cflags_extra ' -I/mbsroot/include -D__USE_MINGW_ANSI_STDIO');
	
	switch (mode)
	{
		case 'debug'
			set cc_cflags = (cc_cflags ' -ggdb');
			break;
		case 'release'
			set cc_cflags = (cc_cflags ' -O3');
			break;
	};
	
	// generate an ID
	set id = (mbs_genid);
	
	// generate a variable inside the Makefile which stores the C source file list,
	// and then figures out .o and .d files
	makerule ('__' id '_SRC := $(subst \\,/,$(shell find ' dirname ' -name "*.cpp"))');
	makerule ('__' id '_OBJ := $(patsubst ' dirname '/%.cpp, obj/' id '/%.o, $(__' id '_SRC))');
	makerule ('__' id '_DEP := $(__' id '_OBJ:.o=.d)');
	
	// rule for generating .d files
	makerule ('obj/' id '/%.d: ' dirname '/%.cpp')
		// first create the directory
		('@"mkdir" -p $(shell dirname $@)')
		
		// now generate the dependency list
		(cxx_compiler ' -c $< -MM -MT $(subst .d,.o,$@) -o $@ ' cc_cflags)
	;
	
	// now for .o files
	makerule ('obj/' id '/%.o: ' dirname '/%.cpp')
		// first create the directory
		('@"mkdir" -p $(shell dirname $@)')
		
		// now generate the object file
		(cxx_compiler ' -c $< -o $@ ' cc_cflags)
	;
	
	set cc_objs = (cc_objs ' $(__' id '_OBJ)');
	set cc_deps = (cc_deps ' $(__' id '_DEP)');
	
	export cc_objs;
	export cc_deps;
};

/**
 * cxx_link
 *
 * Link a C++ binary.
 */
macro cxx_link
{
	meta 'cxx.link' (mbs_file);
	
	set cc_ldflags_extra ?= '';
	set cc_ldflags_libs ?= '';
	
	set cc_ldflags = (cc_ldflags_extra ' ' cc_ldflags_libs);
	
	switch (mode)
	{
		case 'debug'
			set cc_ldflags = (cc_ldflags ' -ggdb');
	};

	// include the dependency files if known
	makerule ('-include ' cc_deps);
	
	// rule for linking
	makerule (mbs_file ': ' cc_objs)
		// first create the directory
		('@"mkdir" -p $(shell dirname $@)')
		
		// link
		(cxx_compiler ' $^ -o $@ ' cc_ldflags)
	;
};

/**
 * cxx_staticlib
 *
 * Make a static library.
 */
macro cxx_staticlib
{
	cc_staticlib;
};

/**
 * cxx_shared
 *
 * Run this macro before any cxx_srcdir or cxx_link macros to mark the target as a shared
 * library.
 */
macro cxx_shared
{
	cc_shared;
};

/**
 * cxx_define <name> <value>
 *
 * #define a name to a value in C/C++. Note that if the value is a string, it must be wrapped in C quotes!
 * Example:
 *	cxx_define 'MY_CONST' '"string"'
 */
macro cxx_define name value
{
	cc_define (name) (value);
};

/**
 * cxx_append_libs <str>
 *
 * Append the specified flags to the linker flags, where <str> should contain only -L and -l
 * switches.
 */
macro cxx_append_libs str
{
	cc_append_libs (str);
};

/**
 * cxx_append_ldflags <str>
 *
 * Append a string to the current list of linker flags.
 */
macro cxx_append_ldflags str
{
	cc_append_ldflags (str);
};

/**
 * cxx_append_cflags <str>
 *
 * Append a string to the current list of C/C++ compiler flags.
 */
macro cxx_append_cflags str
{
	cc_append_cflags (str);
};

/**
 * cxx_incdir <dir>
 *
 * Add an include directory to the project.
 */
macro cxx_incdir dir
{
	meta 'cxx.incdir' (dir);
	cc_incdir (dir);
};