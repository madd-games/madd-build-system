# Introduction to the Madd Build System

MBS is designed to have a similar user interface to GNU Autotools, but be more sane and easier to use for developers, as well as providing more useful features. The idea is very similar: there is a `configure` script, but instead of being written in Bash, it is written in the MBS scripting language. This means that `mbs` must be installed to run it. The developer is expected to edit the `configure` script directly: it is not generated from anything else, and does not need to be updated using commands when something changes, causing less headache to developers. At its heart, the aim of the `configure` script is to generate a `Makefile`.

## Principles and features

* It is _never_ necessary to update any files in the source directory using automatic scripts. This guarantees much simpler integration with collaborative development systems such as Git.
* Build directories can be separate from the source directory. You can have multiple build directories, with completly different configurations: for example a debug build and a release build.
* Cross-compilation is a standard feature.
* And more...

## Simple script: building a C project

Assume we have a directory called `src`, next to the `configure` file, and `src` contains some C files we want to compile and link together. These are both in a directory called `my-project`: this is our source directory, and this is what we share. Next to it, we have a `build-my-project` directory: this is the build directory and is not expected to be shared. Here is a diagram:

```
+-- my-project
| +-- src
| | +-- hello.c
| | +-- second-file.c
| +-- configure
+-- build-my-project
| +-- (current working directory here)
```

The `configure` script for this project would look as follows:

```
#! /usr/bin/mbs

import stdopt;			// module for handling standard options
import cc;			// module for handling C compilation

// This is a package definition. A package is a set of files which can be installed independently
// of others. Most projects have just one package.
package 'my-project' '1.0'
{
	// Define the file <prefix>/bin/my-project. This is the only file in this package, but there
	// could be others with independent build rules.
	//
	// You should always use the 'prefix' variable to allow the user to change the prefix!
	// Furthermore, mbs_ext_bin must be at the end, for systems which require an extension for executables.
	file (prefix '/bin/my-project' mbs_ext_bin)
	{
		// Tell MBS where the source directory containing the C files is.
		// Note how we must use the 'mbs_srcdir' variable.
		cc_srcdir (mbs_srcdir '/src');
		
		// Link into the output file.
		cc_link;
	};
};
```

The comments given should explain what is going on. When inside the `build-my-project` directory, we can configure, build, and install the project like this:

```
../my-project/configure
make
make install
```

This puts the executable at `/usr/local/bin/my-project`. If instead of `/usr/local` you want it under `/usr`, you can configure as follows:

```
../my-project/configure --prefix=/usr
make
make install
```

## Debug vs Release

By default, a release build is made, which can be intalled by an end user. If instead you want to have a debug build (which includes debug symbols etc), pass `--mode=debug` to `configure`. Macros such as `cc_srcdir` and `cc_link` etc take this option into account automatically; so you can use one single `configure` script for both debugging and for releases.

## Libraries

MBS is designed to make library handling easier. Each library should define an MBS module which sets its C compiler and linker flags (using the appropriate macros from `cc`) when the correct macro is executed. Here's an example of linking with the POSIX threading library (`pthread`):

```
#! /usr/bin/mbs

import stdopt;
import cc;
import pthread;

package 'my-project' '1.1'
{
	file (prefix '/bin/my-project')
	{
		// make sure to perform library imports before setting any source directories!
		pthread_import_cc;
		
		cc_srcdir (mbs_srcdir '/src');
		cc_link;
	};
};
