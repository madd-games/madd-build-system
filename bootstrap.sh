#! /bin/sh
# bootstrap.sh -- bootstrap MBS (before MBS is installed)
srcdir="`dirname $0`"
machine="`cc -dumpmachine`"

echo "Source directory: $srcdir"
echo "Machine: $machine"

echo "Step 1: Compile MBS"
cc $srcdir/src/*.c -o mbs-tmp -Wall -Werror -ggdb "-DMBS_SYS=\"$machine\"" '-DMBS_VERSION="<bootstrap>"' || exit 1

echo "Step 2: Compile mbs-merge"
cc $srcdir/merge/*.c -o mbs-merge-tmp -Wall -Werror -ggdb '-DMBS_VERSION="<boostrap>"' || exit 1

echo "Step 3: Run the script using the internal libary"
MBS_PATH="$srcdir/lib" MBS_MERGE=./mbs-merge-tmp ./mbs-tmp $srcdir/configure
