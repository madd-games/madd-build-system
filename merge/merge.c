/*
	Madd Build System

	Copyright (c) 2018, Madd Games.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#endif

void copyFile(const char *srcpath, const char *destpath)
{
	while (memcmp(srcpath, "//", 2) == 0)
	{
		srcpath++;
	}

	while (memcmp(destpath, "//", 2) == 0)
	{
		destpath++;
	}

	FILE *src = fopen(srcpath, "rb");
	if (src == NULL)
	{
		perror(srcpath);
		exit(1);
	};
	
	FILE *dest = fopen(destpath, "wb");
	if (dest == NULL)
	{
		perror(destpath);
		fclose(src);
		exit(1);
	};
	
	char buffer[4096];
	while (!feof(src))
	{
		size_t count = fread(buffer, 1, 4096, src);
		if (ferror(src))
		{
			fprintf(stderr, "read error\n");
			exit(1);
		};
		
		if (fwrite(buffer, 1, count, dest) != count)
		{
			fprintf(stderr, "write error\n");
			exit(1);
		};
	};
	
	fclose(src);
	fclose(dest);

#ifndef _WIN32
	struct stat st;
	if (stat(srcpath, &st) != 0)
	{
		perror(srcpath);
		exit(1);
	};
	
	if (chmod(destpath, (st.st_mode & 0xFFF)) != 0)
	{
		perror(destpath);
		exit(1);
	};
#endif
};

void mergeDir(const char *srcdir, const char *destdir)
{
#ifdef _WIN32
	while (memcmp(srcdir, "//", 2) == 0)
	{
		srcdir++;
	}

	while (memcmp(destdir, "//", 2) == 0)
	{
		destdir++;
	}

	if ((GetFileAttributes(srcdir) & FILE_ATTRIBUTE_DIRECTORY) == 0)
	{
		fprintf(stderr, "%s is not a directory\n", srcdir);
		exit(1);
	};

	CreateDirectory(destdir, NULL);

	char wildcard[strlen(srcdir) + 3];
	sprintf(wildcard, "%s/*", srcdir);

	WIN32_FIND_DATA ffd;
	BOOL moreFiles;
	HANDLE hFindFile;

	for ((moreFiles = ((hFindFile = FindFirstFile(wildcard, &ffd)) != INVALID_HANDLE_VALUE)); moreFiles; (moreFiles = FindNextFile(hFindFile, &ffd)))
	{
		if (strcmp(ffd.cFileName, ".") == 0 || strcmp(ffd.cFileName, "..") == 0)
		{
			continue;
		};

		char srcpath[strlen(srcdir) + strlen(ffd.cFileName) + 3];
		sprintf(srcpath, "%s/%s", srcdir, ffd.cFileName);

		char destpath[strlen(destdir) + strlen(ffd.cFileName) + 3];
		sprintf(destpath, "%s/%s", destdir, ffd.cFileName);

		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			mergeDir(srcpath, destpath);
		}
		else
		{
			copyFile(srcpath, destpath);
		}
	};
#else
	struct stat st;
	if (stat(srcdir, &st) != 0)
	{
		perror(srcdir);
		exit(1);
	};
	
	if (!S_ISDIR(st.st_mode))
	{
		fprintf(stderr, "%s is not a directory\n", srcdir);
		exit(1);
	};
	
	if (mkdir(destdir, 0755) != 0)
	{
		if (errno != EEXIST)
		{
			perror(destdir);
			exit(1);
		};
	};
	
	if (chmod(destdir, (st.st_mode & 0xFFF)) != 0)
	{
		perror(destdir);
		exit(1);
	};
	
	DIR *dirp = opendir(srcdir);
	if (dirp == NULL)
	{
		perror(srcdir);
		exit(1);
	};
	
	struct dirent *ent;
	while ((ent = readdir(dirp)) != NULL)
	{
		if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
		{
			continue;
		};
		
		char srcpath[strlen(srcdir) + strlen(ent->d_name) + 2];
		sprintf(srcpath, "%s/%s", srcdir, ent->d_name);
		
		char destpath[strlen(destdir) + strlen(ent->d_name) + 2];
		sprintf(destpath, "%s/%s", destdir, ent->d_name);
		
		if (lstat(srcpath, &st) != 0)
		{
			perror(srcpath);
			exit(1);
		};
		
		if (S_ISDIR(st.st_mode))
		{
			mergeDir(srcpath, destpath);
		}
		else if (S_ISREG(st.st_mode))
		{
			copyFile(srcpath, destpath);
		}
		else
		{
			fprintf(stderr, "%s is neither a directory nor a regular file\n", srcpath);
			exit(1);
		};
	};
	
	closedir(dirp);
#endif
};

int main(int argc, char *argv[])
{
	if (argc == 2 && strcmp(argv[1], "--version") == 0)
	{
		fprintf(stderr, "Madd Build System %s\n", MBS_VERSION);
		fprintf(stderr, "Copyright (c) 2018, Madd Games. All rights reserved.\n");
		return 0;
	};
	
	if (argc != 3)
	{
		fprintf(stderr, "USAGE:\t%s srcdir destdir\n", argv[0]);
		fprintf(stderr, "\tCopy all contents from directory `srcdir', merging it into `destdir'.\n");
		return 1;
	};
	
	mergeDir(argv[1], argv[2]);
	return 0;
};
